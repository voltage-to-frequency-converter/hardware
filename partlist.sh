#!/bin/bash

part=0
val_old=""

while read line
do
   if [ "$line" == "{" ] 
   then
     part=1
     continue
   elif [ "$line" == "}" ] 
   then
     part=0
     continue
   fi

   if [ $part -eq 1 ]
   then
     val=`echo $line | grep -e 'refdes' -e 'value'`
     if [ "$val" != "$val_old" ]
     then
       if [[ $val == value* ]] 
       then 
          echo $val |sed "s/value=//g"
       elif [[ $val == refdes* ]] 
       then 
          echo $val |sed "s/refdes=//g"
       fi

       val_old=$val
     fi     
   fi   

done < <(cat $1)
    
